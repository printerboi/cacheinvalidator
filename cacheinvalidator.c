#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
MODULE_DESCRIPTION("Module to toggle the cache");
MODULE_AUTHOR("Maximilian Krebs");
MODULE_LICENSE("GPL");

static void disable(void* info){
    __asm__ ("push %rax\n\t"
             "mov    %cr0, %rax\n\t"
             "or    (1 << 30), %rax\n\t"
             "mov    %rax, %cr0\n\t"
             "wbinvd\n\t"
             "pop  %rax\n\t"
            );
}

static void enable(void* info){
    __asm__ ("push %rax\n\t"
             "mov    %cr0, %rax\n\t"
             "and   ~(1 << 30), %rax\n\t"
             "mov   %rax, %cr0\n\t"
             "wbinvd\n\t"
             "pop  %rax\n\t"
            );
}

static int disableCache_init(void)
{
    printk(KERN_INFO "Disabling L1 and L2 caches.\n");

    on_each_cpu(disable, NULL, 0);

    return 0;
}
static void disableCache_exit(void)
{
    printk(KERN_INFO "Enabling L1 and L2 caches.\n");
    on_each_cpu(enable, NULL, 0);
}
module_init(disableCache_init);
module_exit(disableCache_exit);